„Zlepšíme život na venkově,“ slibuje.
Podle zakladatele je předností Rohlik.cz fakt, že vznikl jako internetová firma, **zatímco** konkurence zpravidla začíná kamenným prodejem, k němuž teprve buduje digitální nástavbu.
„My jsme o potravinách na začátku vůbec nic nevěděli,“ směje se Čupr, který čtyři roky před rozjezdem projektu Rohlik.cz zakládal portál Slevomat a o dva roky později rozvážkovou službu DámeJídlo.cz.

---

Nejhorší situace je stále v Lombardii, 2 500 nových pacientů se nachází právě v severní Itálii.
Počet úmrtí z řad nakažených ve Španělsku je **za poslední**ch 24 hodin nižší, než ministerstvo zdravotnictví hlásilo ve středu, kdy za předchozí den vzrostl počet obětí o 738. To byl dosud nejvyšší denní nárůst.
Případů nákazy ale k dnešnímu dni za 24 hodin přibylo 8578. To je více než o den dříve, kdy to bylo 7937 nových potvrzených případů, což byl nejvyšší denní nárůst v absolutních číslech za 24 hodin.

---

Za jediný den zemřelo v celé zemi 662 lidí, celkově nemoci podlehlo 8 165 lidí.
Jen v Lombardii se počet nakažených koronavirem **za poslední** den zvýšil o 2 500.
Také v zemích západní Evropy přes veškeré snahy tamních vlád dál přibývají stížnosti na nedostatek ochranných pomůcek pro pracovníky v první linii boje proti koronaviru.

---

Od středečního rána přibylo v Německu téměř pět tisíc nových případů nákazy koronavirem.
Nyní jich je celkem 36 508. Počet obětí nemoci COVID-19 se **za poslední**ch 24 hodin zvýšil o padesát.
Celkem si tak onemocnění vyžádalo již 198 obětí, uvedl Ústav Roberta Kocha (RKI).

---

A u dluhopisů platí pravidlo, že čím vyšší je jejich cena, tím nižší je jejich výnos (úrok), tedy tím levněji si jejich emitent půjčuje peníze.
Kromě tohoto „psychologického“ působení centrální banka samozřejmě při svých nákupech pouští na trh peníze, což škarohlídi rovněž označují za jejich tištění, **zatímco** korektní ekonomové tomu říkají kvantitativní uvolňování.
Co je důležité v tuto chvíli: pro vládu to znamená radikálně volnější a levnější přístup k dluhu.

---

Je hezké, jak se na sebe lidé přes ty roušky usmívají, pozná se to podle vrásek u očí.
A je ironické, že máma, jíž je 77 a má už léta chronický kašel, který nedokážou vyřešit lékaři ani alternativci v medicíně, na nás přenáší leda svůj naprosto klidný postoj k celé virové situaci, **zatímco** my synové a snachy a bývalé snachy se o ni bojíme, furt jí voláme a to uklidnění asi potřebujeme víc než ona.
A moje dcera, které je 7, vymyslela jeden z možných způsobů, jak si doma s rodinou ukrátit dlouhou chvíli: uspořádat si domácí StarDance, při němž se dá blbnout klidně i několik týdnů.

---

No a zrovna na letošek jsme si s bráchou plánovali, jak bez ženských, jen s dětmi (dohromady 5 dětmi, pokud dobře počítám…) pojedeme v létě na nějakou tuzemskou chalupu a užijeme si tu správně vyklidněnou dovolenou obsahující grilování, výletování, střílení z luku a ze vzduchovky, třeba i koupání a houbaření, záleželo by na stavu klimatu.
No ale **zatímco** já chtěl chalupu objednat už v lednu, brácha to odmítl – on dělá zásadně jenom last minute.
A on je o celých 5 minut starší než já, navíc mi v prosinci prodal své 10 let staré auto a koupil si nové, takže kdo jsem já, abych mu odporoval? Leda tak mladší dvojče.

---

Je jich fůra, tohle mě bude v příštích dnech docela bavit.
A já těmi fotkami můžu pak bavit lidi na sociálních sítích, **zatímco** trčí u monitorů.
Ale to může počkat.

---

Aktuálně je možné si pronajmout byt na Staroměstském náměstí, nebo u Karlova mostu za 15 tisíc korun.
Jen na serveru Sreality.cz se **za poslední**ch 14 dní objevilo víc než 300 nových inzerátů.
Plno bytů je například k dispozici v ulici Dlouhé, tedy v místě, kde byli turisté často nejhlučnější a stejně tak byli slyšet i místní obyvatelé bojující proti nim.

---

Počet potvrzených případů měl ale stoupat výrazně rychleji než těch nezjištěných.
Ke konci dubna model předpokládal zhruba 15 tisíc potvrzených případů onemocnění, **zatímco** počet nezjištěných případů neměl dosáhnout ani šesti tisíc.
K pomalejšímu růstu neodhalených nosičů koronaviru měla přispět jednak nařízená opatření, jako je omezení pohybu nebo nošení roušek, jednak rostoucí množství provedených testů.

---

Peking nařídil vyhoštění třinácti novinářů z New York Times, Washington Post a Wall Street Journal začátkem tohoto měsíce.
Šlo o největší zásah proti zahraničnímu tisku **za poslední**ch několik desítek let, uvádí AFP.
American journalists expelled by China are welcome to set up in Taiwan, foreign minister says, as the island burnishes its free speech credentials 📷@WSJ reporters Julie Wernau (L), Stu Woo, Stephanie Yang at Beijing Capital Airport https://t.co/2SbwuAIZyY pic.twitter.com/x3yLHMiBKQ Thajsko v sobotu oznámilo 109 nových případů koronavirů a jedno úmrtí, celkem země eviduje 1 245 nakažených, šest osob nemoci podlehlo, uvedl mluvčí vládního Střediska pro správu situace ohledně Covid-19. Nových případů přibylo oproti pátečním 91, informuje The Guardian.

---

Zvyšující se cena zlata sice v posledních dnech ztratila na svém tempu, ale jedná se nejspíše pouze o krátkodobý trend.
„Rallye zlata přerušila tradiční korelaci se státními dluhopisy, které prudce poklesly, když někteří investoři odešli z ‚bezpečných přístavů‘ do akcií, **zatímco** jiní prodali více ze strachu z prudkého zvýšení nabídky dluhopisů v důsledku ohlášených obrovských fiskálních stimulačních balíčků, které se stále připravují jak ve Spojených státech či Evropě, ale i v jiných zemích,“ napsal hlavním analytik CFD World Martin Krištoff na web Kurzy.cz.
Připouští možnost, že cena zlata by se na konci roku mohla vyšplhat až k 1 800 dolarů za unci.

---

Tím se přirozeně zvyšuje jejich účinnost, v níž všichni s očima upřenýma na koronavirové číselníky doufáme.
Nejhorší možný scénář z hlediska boje s šířením nákazy by obsahoval sérii opatření, kvůli nimž by se polovina lidí potýkala s nepohodlím roušek, home office a distančního vzdělávání dětí, **zatímco** druhá polovina by na opatření zvysoka kašlala prostě proto, že by nevěřila v jejich smysluplnost.
Dodejme, že druhý možný scénář, v němž by „nedůvěřivá“ polovina lidí byla nucena k poslušnosti silou a represemi, je sice z epidemiologického pohledu rozhodně lepší, ale žít by se v něm asi nechtělo nikomu.

---

V Turecku se k dnešnímu dni potvrdilo 5698 případů nákazy koronavirem, uvedl dnes ministr zdravotnictví Fahrettin Koca.
Počet obětí z řad infikovaných podle něj **za poslední** den vzrostl o 17 na 92.
Albánie o víkendu zavede 40 hodin trvající zákaz vycházení a na nákup bude nově moci jen jeden člen rodiny, který bude od pondělí muset zažádat o povolení prostřednictvím mobilní aplikace.
